var gulp = require('gulp');
var sass = require('gulp-sass');
var size = require('gulp-size');
var pug = require('gulp-pug');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var postcss = require('gulp-postcss');
var uncss = require('postcss-uncss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var runSequence = require('run-sequence');
var del = require('del');
var inlinesource = require('gulp-inline-source');

gulp.task('inline', function () {
    var options = {
        compress: true
    };
    return gulp.src('bin/index.html')
        .pipe(inlinesource(options))
        .pipe(size())
        .pipe(gulp.dest('opt'));
});

// bin-clean
gulp.task('bin-clean', function () {
    return del('bin');
});

// css
gulp.task('css', function () {
    return gulp.src('src/css/style.scss')
        .pipe(sass({outputStyle: 'compressed'})
            .on('>>> SASS COMPILING ERROR: ', sass.logError))
        .pipe(gulp.dest('bin/assets/css'));
});

// css-concat
gulp.task('css-concat', function () {
    return gulp.src([
        'src/css/inc/animate.min.css',
        'src/css/inc/owl.carousel.min.css',
        'src/css/inc/owl.theme.default.min.css',
        'bin/assets/css/style.css'
    ])
        .pipe(concat('style.css'))
        .pipe(gulp.dest('bin/assets/css'));
});

// css-opt
gulp.task('css-opt', function () {
    var plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        uncss({
            html: ['bin/index.html'],
            ignore: ['.main_header.light_bg', '#clients_section .owl-item.active .client_item']
        }),
        cssnano()
    ];
    return gulp.src('bin/assets/css/style.css')
        .pipe(postcss(plugins))
        .pipe(size())
        .pipe(gulp.dest('bin/assets/css'));
});

// js
gulp.task('js', function () {
    return gulp.src([
        'src/js/jquery.js',
        'src/js/bootstrap.js',
        'src/js/owl.carousel.js',
        'src/js/wow.js',
        'src/js/scripts.js'])
        .pipe(concat('pack.js'))
        .pipe(uglify())
        .pipe(gulp.dest('bin/assets/js'));
});

// Pug
gulp.task('html', function () {
    return gulp.src(['src/tpl/**.pug'])
        .pipe(pug({
            pretty: false
        }))
        .pipe(gulp.dest('bin/'));
});

/**gulp.task('compile', ['bin-clean',
    'html',
    'js',
    'css',
    'css-concat',
    'css-opt',
    'inline',
    'bin-clean']);*/

gulp.task('build', function () {
    runSequence(
        'bin-clean',
        'html',
        'js',
        'css',
        'css-concat',
        'css-opt',
        'inline',
        'bin-clean'
    );
});
